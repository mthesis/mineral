import numpy as np



f=np.load("hard.npz",allow_pickle=True)


print(f.files)

x=f["x"]
y=f["y"]


l=[len(xx) for xx in x]

maxn=50

x=[xx for xx,ll in zip(x,l) if ll<=maxn]
y=[yy for yy,ll in zip(y,l) if ll<=maxn]

allx=[]
for xx in x:
  for xxx in xx:
    allx.append(xxx)

allx=list(set(allx))

numx=len(allx)

mp={q:i+1 for i,q in enumerate(allx)}
mp[""]=0
np.savez_compressed("map",q=mp)




def conv(x):
  s=set(x)
  d={ss:0 for ss in s}
  for xx in x:d[xx]+=1
  ret=[]
  for key in d.keys():
    le=d[key]
    for k in np.arange(-1.,1.01,2/le):
      ac=np.zeros((numx+3,))
      ac[mp[key]]=1.0
      ac[-1]=1.0
      ac[-2]=k
      ret.append(ac)
  while len(ret)<maxn:
    ret.append(np.zeros((numx+3,)))
  return np.array(ret)
  

q=np.array([conv(xx) for xx in x])

yo=np.array([yy for yy,qq in zip(y,q) if qq.shape[0]==maxn])


q=np.array([qq for qq in q if qq.shape[0]==maxn])



for qq in q:
  acs=qq.shape
  if acs[0]==50 and acs[1]==71:continue
  print("failed")
  

print(q.shape,yo.shape)
  
  
np.savez_compressed("final",x=q,y=yo)








