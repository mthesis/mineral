import os
import numpy as np
import json

f=["data2/"+q for q in os.listdir("data2")]


def notintag(q):
  intag=0
  ret=""
  for qq in q:
    if qq=="<":intag+=1
    
    if intag==0:ret+=qq
    
    if qq==">":intag-=1
  
  return ret

def counttd(q):
  qq=q.replace("<td","")
  return int((len(q)-len(qq))/3)

def tdsplit(q):
  return ["<td"+qq for qq in q.split("<td")[1:]]

def subana(q):
  q=q.strip()
  cc=counttd(q)
  if not cc==2:return False
  
  
  sp=(tdsplit(q))
  sp=[notintag(qq).strip() for qq in sp]
  
  if sp[0][-1]==":":sp[0]=sp[0][:-1]
  
  if sp[0]=="&nbsp;":return False
  # print(sp[0])
  
  
  return sp[0].strip(),sp[1].strip()

  
  

def anaone(q):
  with open(q,"r") as f:
    p=f.read()
  
  p=p.replace("<sub>","[")
  p=p.replace("</sub>","]")
  p=p.replace("<sup>","{")
  p=p.replace("</sup>","}")
  
  
  
  key1="<tr>"
  key2="</tr>"
  
  entries=[]
  
  while key1 in p:
    p=p[p.find(key1)+len(key1):]
    if not key2 in p:continue
    ac=p[:p.find(key2)]
    p=p[p.find(key2)+len(key2):]
    
    entries.append(ac)
  
  ret={}
  for entry in entries:
    ac=subana(entry)
    if ac==False:continue
    ret[ac[0]]=ac[1]
  
  
  return ret


ret=[]
for ff in f:
  print("did",ff)
  ret.append(anaone(ff))

with open("allofit.json","w") as fil:
  fil.write(json.dumps(ret,indent=2))




# dic1=(anaone(f[0]))

# print(dic1["Hardness"])

# print(dic1.keys())

