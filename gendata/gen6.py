import numpy as np
import json
import math

with open("allofit.json","r") as f:
  q=json.loads(f.read())
  

data={}

data1="Empirical Formula"
data2="Hardness"

tes="Name Pronunciation"


def mod2(q):
  key1=" - "
  if key1 in q:q=q[:q.find(key1)]
  s=q.split("-")
  s=[float(ss) for ss in s]
  s=np.mean(s)
  return s

opts=[]

repl={"&#8226;":"*",
      "\n":"",
      ")[":")*["
      # "([":"[",
      # "])":"]",
      # "":"",
      }


# def mod1(q):
  # print(q)
  
  # for key in repl.keys():
    # q=q.replace(key,repl[key])
  
  
  # q=q.strip()
  # if q[-1]!="]":q=q+"[1]"
  
  
  
  # ret=[]
  # while "[" in q:
    # bef=q[:q.find("[")]
    # q=q[q.find("[")+1:]
    # if not "]" in q:continue
    # mid=q[:q.find("]")]
    # q=q[q.find("]")+1:]
    # # print(mid,"*",bef)
    # opts.append(bef)
    
    # for i in range(int(float(mid))):
      # ret.append(bef)
  # return ret
  
  # exit()
  
  # return q


def iscapital(q):
  return ord(q)>64 and ord(q)<91
def isanticapital(q):
  return ord(q)>64+32 and ord(q)<91+32
def isnumber(q):
  return (ord(q)>47 and ord(q)<58) or q=="."

def delinbrack(q,b1="{",b2="}"):
  intag=0
  ret=""
  for qq in q:
    if qq==b1:intag+=1
    
    if intag==0:ret+=qq
    
    if qq==b2:intag-=1
  
  return ret


def mod1(q):
  
  for key in repl.keys():
    q=q.replace(key,repl[key])

  print(q)
  
  q=delinbrack(q)
  
  if "(" in q or ")" in q or "*" in q:return None

  
  q=q.strip()
  # if q[-1]!="]":q=q+"[1]"
  
  alls=[]
  lastkey=""
  lastnum=""
  for key in q:
    if iscapital(key):
      if len(lastkey)>0:
        alls.append([lastkey,1])
        lastkey=""
      lastkey+=key
    if isanticapital(key):
      lastkey+=key
    if isnumber(key):
      lastnum+=key
    
    if key=="(":
      if len(lastkey)>0:
        alls.append([lastkey,1])
        lastkey=""
    
    if key==")":
      lastkey=mod1(lastkey)
    if key=="[":
      # lastkey=""
      pass
    if key=="]":
      alls.append([lastkey,int(np.rint(float(lastnum)+0.000001))])
      lastnum=""
      lastkey=""
  
  print(alls)
  
  return alls
 
 

# teststr="BaAl[4](AsO[4])"

# print(teststr)
# print(mod1(teststr))

# exit()


data=[]
fail=0

for qq in q:
  k=qq.keys()
  
  if not data1 in k:continue
  if not data2 in k:continue
  
  try:
    
    if tes in qq.keys():
      print("!","working on",qq[tes])
    else:
      print("!","!")
    # print(qq.keys())
    # print("!","!")
    
    m1=mod1(qq[data1])
    m2=mod2(qq[data2])
    if m1 is None:continue#kills most actually
    
    data.append([m1,m2])
  except:
    fail+=1
 

# print(set([data[w] for w in data.keys()]))

print("failed at",fail)
print("got",len(data))

allx=[]
ally=[]

for d in data:
  x=[]
  for dd in d[0]:
    for i in range(dd[1]):
      x.append(dd[0])
  allx.append(x)
  ally.append(d[1])

np.savez_compressed("hard",x=allx,y=ally)
print("done")






